/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef MCAL_DRIVERS_TIMER_DRIVER_TIMER_FUNCTIONS_H_
#define MCAL_DRIVERS_TIMER_DRIVER_TIMER_FUNCTIONS_H_


#include "../../basic_includes/custom_types.h"

// comment-out this line to get normal behavior of Timer
//#define RTOS_COOKIE_

/*
   * Period of each tick/count = pre-scaler / F_CPU.

   - max time wasted in Normal = (pre-scaler / F_CPU) * TOP
   - max time wasted in CTC    = (pre-scaler / F_CPU) * (Compare value + 1)
*/

/*
 * Timer1 run test cases and their results in different modes
 * All INTs were enabled, and corresponding callbacks were registered
 * Callbacks used:
  - t1_ov()
  - t1_cmp_IC()
  - t1_cmp_OC1A()
  - t1_cmp_OC1B()

    Timer1_mode_Normal
    t1_cmp_OC1A
    t1_cmp_OC1B
    t1_ov
    ---------------------------------------------
    Timer1_vidSetCmpValue(120, Timer1_cmp_OCR1A);
    Timer1_vidSetCmpValue(140, Timer1_cmp_OCR1B);
    Timer1_vidSetCmpValue(200, Timer1_cmp_ICR1);


    Timer1_mode_CTC_ICR1
    t1_cmp_OC1A
    t1_cmp_OC1B
    t1_cmp_IC
    ---------------------------------------------
    Timer1_vidSetCmpValue(120, Timer1_cmp_OCR1A);
    Timer1_vidSetCmpValue(140, Timer1_cmp_OCR1B);
    Timer1_vidSetCmpValue(200, Timer1_cmp_ICR1);


    Timer1_mode_CTC_OCR1A
    t1_cmp_OC1B
    t1_cmp_OC1A
    ---------------------------------------------
    Timer1_vidSetCmpValue(120, Timer1_cmp_ICR1);
    Timer1_vidSetCmpValue(140, Timer1_cmp_OCR1B);
    Timer1_vidSetCmpValue(200, Timer1_cmp_OCR1A);


    Timer1_mode_PWM_ICR1
    t1_cmp_OC1A
    t1_cmp_OC1B
    t1_cmp_IC
    t1_ov
    ---------------------------------------------
    Timer1_vidSetCmpValue(120, Timer1_cmp_OCR1A);
    Timer1_vidSetCmpValue(140, Timer1_cmp_OCR1B);
    Timer1_vidSetCmpValue(200, Timer1_cmp_ICR1);


    Timer1_mode_PWM_OCR1A
    t1_cmp_OC1B
    t1_cmp_OC1A
    t1_ov
    ---------------------------------------------
    Timer1_vidSetCmpValue(120, Timer1_cmp_ICR1);
    Timer1_vidSetCmpValue(140, Timer1_cmp_OCR1B);
    Timer1_vidSetCmpValue(200, Timer1_cmp_OCR1A);


    Timer1_mode_PWM_Phase_correct_ICR1
    t1_cmp_OC1A
    t1_cmp_OC1B
    t1_cmp_IC
    t1_cmp_OC1B
    t1_cmp_OC1A
    t1_ov
    ---------------------------------------------
    Timer1_vidSetCmpValue(120, Timer1_cmp_OCR1A);
    Timer1_vidSetCmpValue(140, Timer1_cmp_OCR1B);
    Timer1_vidSetCmpValue(200, Timer1_cmp_ICR1);


    Timer1_mode_PWM_Phase_correct_OCR1A
    t1_cmp_OC1B
    t1_cmp_OC1B
    t1_ov
    ---------------------------------------------
    Timer1_vidSetCmpValue(120, Timer1_cmp_ICR1);
    Timer1_vidSetCmpValue(140, Timer1_cmp_OCR1B);
    Timer1_vidSetCmpValue(200, Timer1_cmp_OCR1A);
*/

/*
 * Timer0 run test cases and their results in different modes
 * All INTs were enabled, and corresponding callbacks were registered
 * Callbacks used:
  - t0_ov()
  - t0_cmp()

    Timer_mode_Normal
    t0_cmp
    t0_ov
    -----------------------------
    Timer_vidSetCmpValue(127, 0);


    Timer_mode_CTC
    t0_cmp
    -----------------------------
    Timer_vidSetCmpValue(127, 0);


    Timer_mode_PWM
    t0_cmp
    t0_ov
    -----------------------------
    Timer_vidSetCmpValue(127, 0);


    Timer_mode_PWM_Phase_correct
    t0_cmp
    t0_cmp
    t0_ov
    -----------------------------
    Timer_vidSetCmpValue(127, 0);
*/

// timer0 prescaler
typedef enum
{
    // internal clock/prescaler
    Timer0_prescaler_off      = 0b00000000,
    Timer0_prescaler_div_1    = 0b00000001,
    Timer0_prescaler_div_8    = 0b00000010,
    Timer0_prescaler_div_64   = 0b00000011,
    Timer0_prescaler_div_256  = 0b00000100,
    Timer0_prescaler_div_1024 = 0b00000101,

    // external clock
    Timer0_prescaler_falling_edge_external = 0b00000110,
    Timer0_prescaler_rising_edge_external  = 0b00000111
} Timer0_prescaler_t;

// timer2 prescaler
typedef enum
{
    // internal clock/prescaler
    Timer2_prescaler_off      = 0b00000000,
    Timer2_prescaler_div_1    = 0b00000001,
    Timer2_prescaler_div_8    = 0b00000010,
	Timer2_prescaler_div_32   = 0b00000011,
    Timer2_prescaler_div_64   = 0b00000100,
	Timer2_prescaler_div_128  = 0b00000101,
    Timer2_prescaler_div_256  = 0b00000110,
    Timer2_prescaler_div_1024 = 0b00000111
} Timer2_prescaler_t;

// timer1 prescaler
typedef enum
{
    // internal clock/prescaler
    Timer1_prescaler_off      = 0b00000000,
    Timer1_prescaler_div_1    = 0b00000001,
    Timer1_prescaler_div_8    = 0b00000010,
    Timer1_prescaler_div_64   = 0b00000011,
    Timer1_prescaler_div_256  = 0b00000100,
    Timer1_prescaler_div_1024 = 0b00000101
} Timer1_prescaler_t;

// timer mode (normal, CTC, PWM, PWM Phase Correct)
typedef enum
{
    // counting direction: incrementing, no counter clear is performed, Top value = 0xFF,
    // Overflow Flag (TOV0) will be set in the same timer clock-cycle as the TCNT0 becomes zero (after 255 ticks),
    // also, compare-match flag is set in the clock cycle after compare-match happens, if compare-match INT is enabled
	Timer_mode_Normal            = 0b00000000,
	// counter is cleared to zero when the counter value (TCNT0) matches the OCR0,
	// OCR0 defines the top value for the counter,
	// compare-match flag is set in the clock cycle after compare-match happens
	Timer_mode_CTC               = 0b00001000,
	// counts from BOTTOM to MAX then restarts from BOTTOM.
	// In non-inverting Compare Output mode, the Output Compare (OC0) is cleared
	// on the compare match between TCNT0 and OCR0, and set at BOTTOM.
	// Overflow Flag (TOV0) will be set in the same timer clock-cycle as the TCNT0 becomes zero (after 255 ticks),
    // also, compare-match flag is set in the clock cycle after compare-match happens, if compare-match INT is enabled
	Timer_mode_PWM               = 0b01001000,
	// The counter counts repeatedly from BOTTOM to MAX and then from MAX to BOTTOM.
	// In non-inverting Compare Output mode, the Output Compare (OC0)
	// is cleared on the compare-match between TCNT0 and OCR0 while up-counting,
	// and set on the compare match while down-counting.
	// Overflow Flag (TOV0) will be set in the same timer clock-cycle as the TCNT0 becomes zero (after counting downward),
    // also, compare-match flag is set 2 times in the clock cycle after compare-match happens, one time when counting upward
	// and another time when counting downward
	Timer_mode_PWM_Phase_correct = 0b01000000
} Timer_mode_t;

// timer1 mode (normal, CTC, PWM, PWM Phase Correct)
typedef enum
{
    // counting direction: incrementing, no counter clear is performed, Top value = 0xFFFF,
    // Overflow Flag (TOV) will be set in the same timer clock-cycle as the TCNT1 becomes zero
    Timer1_mode_Normal                      = 0b00000000,
    // counter is cleared to zero when the counter value (TCNT1) matches the OCR1A,
    // OCR1A defines the top value for the counter
    Timer1_mode_CTC_TOP_OCR1A               = 0b00001000,
    // counter is cleared to zero when the counter value (TCNT1) matches the ICR1,
    // ICR1 defines the top value for the counter
    Timer1_mode_CTC_TOP_ICR1                = 0b00011000,
    // provides a high frequency waveform generation option,
    // counts from BOTTOM to TOP then restarts from BOTTOM.
	// OCR1A defines TOP
    // In non-inverting Compare Output mode, the Output Compare (OC1) is cleared
    // on the compare match between TCNT1 and OCR1, and set at BOTTOM.
    Timer1_mode_PWM_TOP_OCR1A               = 0b00011011,
    // provides a high frequency waveform generation option,
    // counts from BOTTOM to TOP then restarts from BOTTOM.
	// ICR1 defines TOP
    // In non-inverting Compare Output mode, the Output Compare (OC1) is cleared
    // on the compare match between TCNT1 and ICR1, and set at BOTTOM.
    Timer1_mode_PWM_TOP_ICR1                = 0b00011010,
    // The counter counts repeatedly from BOTTOM to MAX and then from MAX to BOTTOM.
    // In non-inverting Compare Output mode, the Output Compare (OC1)
    // is cleared on the compare-match between TCNT1 and OCR1 while up-counting,
    // and set on the compare match while down-counting.
    Timer1_mode_PWM_Phase_correct_TOP_OCR1A = 0b00010011,
    // The counter counts repeatedly from BOTTOM to MAX and then from MAX to BOTTOM.
    // In non-inverting Compare Output mode, the Output Compare (OC0)
    // is cleared on the compare-match between TCNT0 and OCR0 while up-counting,
    // and set on the compare match while down-counting.
    Timer1_mode_PWM_Phase_correct_TOP_ICR1  = 0b00010010
} Timer1_mode_t;

// output wave mode on compare-match (toggle, set, clear)
typedef enum
{
	Timer_wave_off    = 0b00000000,
	Timer_wave_toggle = 0b00010000,
	Timer_wave_clear  = 0b00100000,
	Timer_wave_set    = 0b00110000
} Timer_wave_t;

// output wave mode on compare-match (A / B) (toggle, set, clear)
typedef enum
{
    Timer1_wave_off    = 0b00000000,
    Timer1_wave_toggle = 0b01010000,
    Timer1_wave_clear  = 0b10100000,
    Timer1_wave_set    = 0b11110000
} Timer1_wave_t;

// timer1 output wave channel
typedef enum
{
    Timer1_channelA,
    Timer1_channelB
} Timer1_channel_t;

// timer1 cmp register selector
typedef enum
{
    Timer1_cmp_OCR1A,
    Timer1_cmp_ICR1,
    Timer1_cmp_OCR1B
} Timer1_cmp_reg_t;

// timer1 input capture edge
typedef enum
{
    Timer1_IC_edge_falling,
    Timer1_IC_edge_rising
} Timer1_IC_edge_t;

typedef void (*Timer_CB_t)(void);

void Timer_vidInit(const u8 u8TimerClockCpy, Timer_mode_t enumTimerModeCpy, const u8 u8TimerNCpy);
void Timer1_vidInit(Timer1_prescaler_t enumTimer1ClockCpy, Timer1_mode_t enumTimer1ModeCpy);

// --- setup --- //
void Timer_vidSetClock(const u8 u8TimerClockCpy, const u8 u8TimerNCpy);
void Timer1_vidSetClock(Timer1_prescaler_t enumTimer1ClockCpy);

void Timer_vidSetMode(Timer_mode_t enumTimerModeCpy, const u8 u8TimerNCpy);
void Timer1_vidSetMode(Timer1_mode_t enumTimer1ModeCpy);

void Timer_vidSetWaveMode(Timer_wave_t enumTimerWaveCpy, const u8 u8TimerNCpy);
void Timer1_vidSetWaveMode(Timer1_wave_t enumTimer1WaveCpy, Timer1_channel_t enumTimer1CHCpy);

void Timer1_vidSetICedge(Timer1_IC_edge_t enumTimer1ICedgeCpy);

void Timer1_vidEnableICfilter(void);
void Timer1_vidDisableICfilter(void);
// ------------- //

void Timer_vidEnable(const u8 u8TimerClockCpy, const u8 u8TimerNCpy);
void Timer1_vidEnable(Timer1_prescaler_t enumTimer1ClockCpy);
void Timer_vidDisable(const u8 u8TimerNCpy);
void Timer1_vidDisable(void);

void Timer_vidSetValue(const u8 u8ValueCpy, const u8 u8TimerNCpy);
void Timer1_vidSetValue(const u16 u16ValueCpy);
void Timer_vidSetCmpValue(const u8 u8ValCpy, const u8 u8TimerNCpy);
void Timer1_vidSetCmpValue(const u16 u16ValCpy, Timer1_cmp_reg_t enumTimer1CmpRegCpy);

void Timer_vidResetValue(const u8 u8TimerNCpy);
void Timer1_vidResetValue(void);
void Timer_vidResetCmpValue(const u8 u8TimerNCpy);
void Timer1_vidResetCmpValue(Timer1_cmp_reg_t enumTimer1CmpRegCpy);

u8 Timer_u8GetValue(const u8 u8TimerNCpy);
u16 Timer1_u16GetValue(void);
u8 Timer_u8GetCmpValue(const u8 u8TimerNCpy);
u16 Timer1_u16GetCmpValue(Timer1_cmp_reg_t enumTimer1CmpRegCpy);

void Timer_vidResetOverflowFlag(const u8 u8TimerNCpy);
void Timer1_vidResetOverflowFlag(void);
void Timer_vidResetCmpMatchFlag(const u8 u8TimerNCpy);
void Timer1_vidResetCmpMatchFlag(Timer1_cmp_reg_t enumTimer1CmpRegCpy);

u8 Timer_u8isOverflow(const u8 u8TimerNCpy);
u8 Timer1_u8isOverflow(void);
u8 Timer_u8isCmpMatch(const u8 u8TimerNCpy);
u8 Timer1_u8isCmpMatch(Timer1_cmp_reg_t enumTimer1CmpRegCpy);

// --- interrupts --- //
// *** compare-match *** //
void Timer_vidEnableCmpINT(const u8 u8TimerNCpy);
void Timer1_vidEnableCmpINT(Timer1_cmp_reg_t enumTimer1CmpRegCpy);
void Timer_vidDisableCmpINT(const u8 u8TimerNCpy);
void Timer1_vidDisableCmpINT(Timer1_cmp_reg_t enumTimer1CmpRegCpy);
// ********************* //

// *** overflow *** //
void Timer_vidEnableOverflowINT(const u8 u8TimerNCpy);
void Timer1_vidEnableOverflowINT(void);
void Timer_vidDisableOverflowINT(const u8 u8TimerNCpy);
void Timer1_vidDisableOverflowINT(void);
// **************** //
// ------------------ //

// --- callbacks --- //
// *** compare-match *** //
void Timer_vidRegisterCB_Cmp(const Timer_CB_t CBfuncCpy, const u8 u8TimerNCpy);
void Timer1_vidRegisterCB_Cmp(const Timer_CB_t CBfuncCpy, Timer1_cmp_reg_t enumTimer1CmpRegCpy);
void Timer_vidDeregisterCB_Cmp(const u8 u8TimerNCpy);
void Timer1_vidDeregisterCB_Cmp(Timer1_cmp_reg_t enumTimer1CmpRegCpy);
// ********************* //

// *** overflow *** //
void Timer_vidRegisterCB_Overflow(const Timer_CB_t CBfuncCpy, const u8 u8TimerNCpy);
void Timer1_vidRegisterCB_Overflow(const Timer_CB_t CBfuncCpy);
void Timer_vidDeregisterCB_Overflow(const u8 u8TimerNCpy);
void Timer1_vidDeregisterCB_Overflow(void);
// **************** //
// ----------------- //


#endif /* MCAL_DRIVERS_TIMER_DRIVER_TIMER_FUNCTIONS_H_ */

