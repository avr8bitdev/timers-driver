/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "Timer_functions.h"
#include "../Peripherals_addresses.h"
#include "../DIO_driver/DIO_functions.h"
#include "../../basic_includes/bit_manip.h"
#include <avr/interrupt.h>


// --- internal --- //
// callback functions
static Timer_CB_t Timer0_CB_Cmp      = 0;
static Timer_CB_t Timer0_CB_Overflow = 0;

static Timer_CB_t Timer2_CB_Cmp      = 0;
static Timer_CB_t Timer2_CB_Overflow = 0;

static Timer_CB_t Timer1_CB_CmpA     = 0;
static Timer_CB_t Timer1_CB_CmpIC    = 0;
static Timer_CB_t Timer1_CB_CmpB     = 0;
static Timer_CB_t Timer1_CB_Overflow = 0;
// ---------------- //

void Timer_vidInit(const u8 u8TimerClockCpy, Timer_mode_t enumTimerModeCpy, const u8 u8TimerNCpy)
{
    // disable timer
    Timer_vidDisable(u8TimerNCpy);

    // disable INTs
    Timer_vidDisableOverflowINT(u8TimerNCpy);
    Timer_vidDisableCmpINT(u8TimerNCpy);

    // reset counters
    Timer_vidResetValue(u8TimerNCpy);
    Timer_vidResetCmpValue(u8TimerNCpy);

    // reset flags
    Timer_vidResetOverflowFlag(u8TimerNCpy);
    Timer_vidResetCmpMatchFlag(u8TimerNCpy);

    Timer_vidSetMode(enumTimerModeCpy, u8TimerNCpy);

    // reset callbacks
    switch (u8TimerNCpy)
    {
        case 0:
            Timer0_CB_Cmp = 0;
            Timer0_CB_Overflow = 0;
        break;

        case 2:
            Timer2_CB_Cmp = 0;
            Timer2_CB_Overflow = 0;
        break;
    }

    // turn on
    Timer_vidSetClock(u8TimerClockCpy, u8TimerNCpy);
}

void Timer1_vidInit(Timer1_prescaler_t enumTimer1ClockCpy, Timer1_mode_t enumTimer1ModeCpy)
{
    // disable timer
    Timer1_vidDisable();

    // disable INTs
    Timer1_vidDisableOverflowINT();
    Timer1_vidDisableCmpINT(Timer1_cmp_OCR1A);
    Timer1_vidDisableCmpINT(Timer1_cmp_ICR1);
    Timer1_vidDisableCmpINT(Timer1_cmp_OCR1B);

    // disable Input Capture filter
    Timer1_vidDisableICfilter();

    // reset counters
    Timer1_vidResetValue();
    Timer1_vidResetCmpValue(Timer1_cmp_OCR1A);
    Timer1_vidResetCmpValue(Timer1_cmp_ICR1);
    Timer1_vidResetCmpValue(Timer1_cmp_OCR1B);

    // reset flags
    Timer1_vidResetOverflowFlag();
    Timer1_vidResetCmpMatchFlag(Timer1_cmp_OCR1A);
    Timer1_vidResetCmpMatchFlag(Timer1_cmp_ICR1);
    Timer1_vidResetCmpMatchFlag(Timer1_cmp_OCR1B);

    Timer1_vidSetMode(enumTimer1ModeCpy);

    // reset callbacks
    Timer1_CB_CmpA     = 0;
    Timer1_CB_CmpIC    = 0;
    Timer1_CB_CmpB     = 0;
    Timer1_CB_Overflow = 0;

    // turn on
    Timer1_vidSetClock(enumTimer1ClockCpy);
}

// --- setup --- //
void Timer_vidSetClock(const u8 u8TimerClockCpy, const u8 u8TimerNCpy)
{
    switch (u8TimerNCpy)
    {
        case 0:
            BIT_ASSIGN( TIMER0_CTRL, 0, BIT_GET(u8TimerClockCpy, 0) );
            BIT_ASSIGN( TIMER0_CTRL, 1, BIT_GET(u8TimerClockCpy, 1) );
            BIT_ASSIGN( TIMER0_CTRL, 2, BIT_GET(u8TimerClockCpy, 2) );
        break;

        case 2:
            BIT_ASSIGN( TIMER2_CTRL, 0, BIT_GET(u8TimerClockCpy, 0) );
            BIT_ASSIGN( TIMER2_CTRL, 1, BIT_GET(u8TimerClockCpy, 1) );
            BIT_ASSIGN( TIMER2_CTRL, 2, BIT_GET(u8TimerClockCpy, 2) );
        break;
    }
}

void Timer1_vidSetClock(Timer1_prescaler_t enumTimer1ClockCpy)
{
    BIT_ASSIGN( TIMER1B_CTRL, 0, BIT_GET(enumTimer1ClockCpy, 0) );
    BIT_ASSIGN( TIMER1B_CTRL, 1, BIT_GET(enumTimer1ClockCpy, 1) );
    BIT_ASSIGN( TIMER1B_CTRL, 2, BIT_GET(enumTimer1ClockCpy, 2) );
}

void Timer_vidSetMode(Timer_mode_t enumTimerModeCpy, const u8 u8TimerNCpy)
{
    switch (u8TimerNCpy)
    {
        case 0:
            BIT_ASSIGN( TIMER0_CTRL, 3, BIT_GET(enumTimerModeCpy, 3) );
            BIT_ASSIGN( TIMER0_CTRL, 6, BIT_GET(enumTimerModeCpy, 6) );
        break;

        case 2:
            BIT_ASSIGN( TIMER2_CTRL, 3, BIT_GET(enumTimerModeCpy, 3) );
            BIT_ASSIGN( TIMER2_CTRL, 6, BIT_GET(enumTimerModeCpy, 6) );
        break;
    }
}

void Timer1_vidSetMode(Timer1_mode_t enumTimer1ModeCpy)
{
    BIT_ASSIGN( TIMER1A_CTRL, 0, BIT_GET(enumTimer1ModeCpy, 0) );
    BIT_ASSIGN( TIMER1A_CTRL, 1, BIT_GET(enumTimer1ModeCpy, 1) );

    BIT_ASSIGN( TIMER1B_CTRL, 3, BIT_GET(enumTimer1ModeCpy, 3) );
    BIT_ASSIGN( TIMER1B_CTRL, 4, BIT_GET(enumTimer1ModeCpy, 4) );
}

void Timer_vidSetWaveMode(Timer_wave_t enumTimerWaveCpy, const u8 u8TimerNCpy)
{
    switch (u8TimerNCpy)
    {
        case 0:
            DIO_vidSet_pinDirection(port_B, 3, OUTPUT);
            BIT_ASSIGN( TIMER0_CTRL, 4, BIT_GET(enumTimerWaveCpy, 4) );
            BIT_ASSIGN( TIMER0_CTRL, 5, BIT_GET(enumTimerWaveCpy, 5) );
        break;

        case 2:
            DIO_vidSet_pinDirection(port_D, 7, OUTPUT);
            BIT_ASSIGN( TIMER2_CTRL, 4, BIT_GET(enumTimerWaveCpy, 4) );
            BIT_ASSIGN( TIMER2_CTRL, 5, BIT_GET(enumTimerWaveCpy, 5) );
        break;
    }
}

void Timer1_vidSetWaveMode(Timer1_wave_t enumTimer1WaveCpy, Timer1_channel_t enumTimer1CHCpy)
{
    switch (enumTimer1CHCpy)
    {
        case Timer1_channelA:
            DIO_vidSet_pinDirection(port_D, 5, OUTPUT);
            BIT_ASSIGN( TIMER1A_CTRL, 6, BIT_GET(enumTimer1WaveCpy, 6) );
            BIT_ASSIGN( TIMER1A_CTRL, 7, BIT_GET(enumTimer1WaveCpy, 7) );
        break;

        case Timer1_channelB:
            DIO_vidSet_pinDirection(port_D, 4, OUTPUT);
            BIT_ASSIGN( TIMER1A_CTRL, 4, BIT_GET(enumTimer1WaveCpy, 4) );
            BIT_ASSIGN( TIMER1A_CTRL, 5, BIT_GET(enumTimer1WaveCpy, 5) );
        break;
    }
}

inline void Timer1_vidSetICedge(Timer1_IC_edge_t enumTimer1ICedgeCpy)
{
    switch (enumTimer1ICedgeCpy)
    {
        case Timer1_IC_edge_falling:
            BIT_CLEAR(TIMER1B_CTRL, 6);
        break;

        case Timer1_IC_edge_rising:
            BIT_SET(TIMER1B_CTRL, 6);
        break;
    }
}

inline void Timer1_vidEnableICfilter(void)
{
    BIT_SET(TIMER1B_CTRL, 7);
}

inline void Timer1_vidDisableICfilter(void)
{
    BIT_CLEAR(TIMER1B_CTRL, 7);
}
// ------------- //

inline void Timer_vidEnable(const u8 u8TimerClockCpy, const u8 u8TimerNCpy)
{
    Timer_vidSetClock(u8TimerClockCpy, u8TimerNCpy);
}

inline void Timer1_vidEnable(Timer1_prescaler_t enumTimer1ClockCpy)
{
    Timer1_vidSetClock(enumTimer1ClockCpy);
}

inline void Timer_vidDisable(const u8 u8TimerNCpy)
{
	switch (u8TimerNCpy)
	{
		case 0:
			Timer_vidSetClock(Timer0_prescaler_off, u8TimerNCpy);
		break;

		case 2:
			Timer_vidSetClock(Timer2_prescaler_off, u8TimerNCpy);
		break;
	}
}

inline void Timer1_vidDisable(void)
{
    Timer1_vidSetClock(Timer1_prescaler_off);
}

inline void Timer_vidSetValue(const u8 u8ValueCpy, const u8 u8TimerNCpy)
{
    switch (u8TimerNCpy)
    {
        case 0:
            TIMER0_VALUE = u8ValueCpy;
        break;

        case 2:
            TIMER2_VALUE = u8ValueCpy;
        break;
    }
}

inline void Timer1_vidSetValue(const u16 u16ValueCpy)
{
    TIMER1_VALUE = u16ValueCpy;
}

inline void Timer_vidSetCmpValue(const u8 u8ValCpy, const u8 u8TimerNCpy)
{
    switch (u8TimerNCpy)
    {
        case 0:
            TIMER0_CMP = u8ValCpy;
        break;

        case 2:
            TIMER2_CMP = u8ValCpy;
        break;
    }
}

inline void Timer1_vidSetCmpValue(const u16 u16ValCpy, Timer1_cmp_reg_t enumTimer1CmpRegCpy)
{
    switch (enumTimer1CmpRegCpy)
    {
        case Timer1_cmp_OCR1A:
            TIMER1A_CMP = u16ValCpy;
        break;

        case Timer1_cmp_ICR1:
            TIMER1_INPUT_CAPTURE_VALUE = u16ValCpy;
        break;

        case Timer1_cmp_OCR1B:
            TIMER1B_CMP = u16ValCpy;
        break;

    }
}

inline void Timer_vidResetValue(const u8 u8TimerNCpy)
{
    Timer_vidSetValue(0, u8TimerNCpy);
}

inline void Timer1_vidResetValue(void)
{
    Timer1_vidSetValue(0);
}

inline void Timer_vidResetCmpValue(const u8 u8TimerNCpy)
{
    Timer_vidSetCmpValue(0, u8TimerNCpy);
}

inline void Timer1_vidResetCmpValue(Timer1_cmp_reg_t enumTimer1CmpRegCpy)
{
    Timer1_vidSetCmpValue(0, enumTimer1CmpRegCpy);
}

inline u8 Timer_u8GetValue(const u8 u8TimerNCpy)
{
    switch (u8TimerNCpy)
    {
        case 0:
            return TIMER0_VALUE;
        break;

        case 2:
            return TIMER2_VALUE;
        break;

        default:
            return 0;
        break;
    }
}

inline u16 Timer1_u16GetValue(void)
{
    return TIMER1_VALUE;
}

inline u8 Timer_u8GetCmpValue(const u8 u8TimerNCpy)
{
    switch (u8TimerNCpy)
    {
        case 0:
            return TIMER0_CMP;
        break;

        case 2:
            return TIMER2_CMP;
        break;

        default:
            return 0;
        break;
    }
}

inline u16 Timer1_u16GetCmpValue(Timer1_cmp_reg_t enumTimer1CmpRegCpy)
{
     switch (enumTimer1CmpRegCpy)
    {
        case Timer1_cmp_OCR1A:
            return TIMER1A_CMP;

        case Timer1_cmp_ICR1:
            return TIMER1_INPUT_CAPTURE_VALUE;

        case Timer1_cmp_OCR1B:
            return TIMER1B_CMP;

        default:
            return 0;
    }
}

inline void Timer_vidResetOverflowFlag(const u8 u8TimerNCpy)
{
    switch (u8TimerNCpy)
    {
        case 0:
            BIT_SET(TIMER_INT_STATUS, 0);
        break;

        case 2:
            BIT_SET(TIMER_INT_STATUS, 6);
        break;
    }
}

inline void Timer1_vidResetOverflowFlag(void)
{
    BIT_SET(TIMER_INT_STATUS, 2);
}

inline void Timer_vidResetCmpMatchFlag(const u8 u8TimerNCpy)
{
    switch (u8TimerNCpy)
    {
        case 0:
            BIT_SET(TIMER_INT_STATUS, 1);
        break;

        case 2:
            BIT_SET(TIMER_INT_STATUS, 7);
        break;
    }
}

inline void Timer1_vidResetCmpMatchFlag(Timer1_cmp_reg_t enumTimer1CmpRegCpy)
{
    switch (enumTimer1CmpRegCpy)
    {
        case Timer1_cmp_OCR1A:
            BIT_SET(TIMER_INT_STATUS, 4);
        break;

        case Timer1_cmp_ICR1:
            BIT_SET(TIMER_INT_STATUS, 5);
        break;

        case Timer1_cmp_OCR1B:
            BIT_SET(TIMER_INT_STATUS, 3);
        break;
    }
}

inline u8 Timer_u8isOverflow(const u8 u8TimerNCpy)
{
    switch (u8TimerNCpy)
    {
        case 0:
            return BIT_GET(TIMER_INT_STATUS, 0);
        break;

        case 2:
            return BIT_GET(TIMER_INT_STATUS, 6);
        break;

        default:
            return 0;
        break;
    }
}

inline u8 Timer1_u8isOverflow(void)
{
    return BIT_GET(TIMER_INT_STATUS, 2);
}

inline u8 Timer_u8isCmpMatch(const u8 u8TimerNCpy)
{
    switch (u8TimerNCpy)
    {
        case 0:
            return BIT_GET(TIMER_INT_STATUS, 1);
        break;

        case 2:
            return BIT_GET(TIMER_INT_STATUS, 7);
        break;

        default:
            return 0;
        break;
    }
}

inline u8 Timer1_u8isCmpMatch(Timer1_cmp_reg_t enumTimer1CmpRegCpy)
{
    switch (enumTimer1CmpRegCpy)
    {
        case Timer1_cmp_OCR1A:
            return BIT_GET(TIMER_INT_STATUS, 4);

        case Timer1_cmp_ICR1:
            return BIT_GET(TIMER_INT_STATUS, 5);

        case Timer1_cmp_OCR1B:
            return BIT_GET(TIMER_INT_STATUS, 3);

        default:
            return 0;
    }
}

// --- interrupts --- //
// *** compare-match *** //
inline void Timer_vidEnableCmpINT(const u8 u8TimerNCpy)
{
    switch (u8TimerNCpy)
    {
        case 0:
            BIT_SET(TIMER_INT_MASK, 1);
        break;

        case 2:
            BIT_SET(TIMER_INT_MASK, 7);
        break;
    }
}

inline void Timer1_vidEnableCmpINT(Timer1_cmp_reg_t enumTimer1CmpRegCpy)
{
    switch (enumTimer1CmpRegCpy)
    {
        case Timer1_cmp_OCR1A:
            BIT_SET(TIMER_INT_MASK, 4);
        break;

        case Timer1_cmp_ICR1:
            BIT_SET(TIMER_INT_MASK, 5);
        break;

        case Timer1_cmp_OCR1B:
            BIT_SET(TIMER_INT_MASK, 3);
        break;
    }
}

inline void Timer_vidDisableCmpINT(const u8 u8TimerNCpy)
{
    switch (u8TimerNCpy)
    {
        case 0:
            BIT_CLEAR(TIMER_INT_MASK, 1);
        break;

        case 2:
            BIT_CLEAR(TIMER_INT_MASK, 7);
        break;
    }
}

inline void Timer1_vidDisableCmpINT(Timer1_cmp_reg_t enumTimer1CmpRegCpy)
{
    switch (enumTimer1CmpRegCpy)
    {
        case Timer1_cmp_OCR1A:
            BIT_CLEAR(TIMER_INT_MASK, 4);
        break;

        case Timer1_cmp_ICR1:
            BIT_CLEAR(TIMER_INT_MASK, 5);
        break;

        case Timer1_cmp_OCR1B:
            BIT_CLEAR(TIMER_INT_MASK, 3);
        break;
    }
}
// ********************* //

// *** overflow *** //
inline void Timer_vidEnableOverflowINT(const u8 u8TimerNCpy)
{
    switch (u8TimerNCpy)
    {
        case 0:
            BIT_SET(TIMER_INT_MASK, 0);
        break;

        case 2:
            BIT_SET(TIMER_INT_MASK, 6);
        break;
    }
}

inline void Timer1_vidEnableOverflowINT(void)
{
    BIT_SET(TIMER_INT_MASK, 2);
}

inline void Timer_vidDisableOverflowINT(const u8 u8TimerNCpy)
{
    switch (u8TimerNCpy)
    {
        case 0:
            BIT_CLEAR(TIMER_INT_MASK, 0);
        break;

        case 2:
            BIT_CLEAR(TIMER_INT_MASK, 6);
        break;
    }
}

inline void Timer1_vidDisableOverflowINT(void)
{
    BIT_CLEAR(TIMER_INT_MASK, 2);
}
// **************** //
// ------------------ //

// --- callbacks --- //
// *** compare-match *** //
inline void Timer_vidRegisterCB_Cmp(const Timer_CB_t CBfuncCpy, const u8 u8TimerNCpy)
{
    switch (u8TimerNCpy)
    {
        case 0:
            Timer0_CB_Cmp = CBfuncCpy;
        break;

        case 2:
            Timer2_CB_Cmp = CBfuncCpy;
        break;
    }
}

inline void Timer1_vidRegisterCB_Cmp(const Timer_CB_t CBfuncCpy, Timer1_cmp_reg_t enumTimer1CmpRegCpy)
{
    switch (enumTimer1CmpRegCpy)
    {
        case Timer1_cmp_OCR1A:
            Timer1_CB_CmpA = CBfuncCpy;
        break;

        case Timer1_cmp_ICR1:
            Timer1_CB_CmpIC = CBfuncCpy;
        break;

        case Timer1_cmp_OCR1B:
            Timer1_CB_CmpB = CBfuncCpy;
        break;
    }
}

inline void Timer_vidDeregisterCB_Cmp(const u8 u8TimerNCpy)
{
    switch (u8TimerNCpy)
    {
        case 0:
            Timer0_CB_Cmp = 0;
        break;

        case 2:
            Timer2_CB_Cmp = 0;
        break;
    }
}

inline void Timer1_vidDeregisterCB_Cmp(Timer1_cmp_reg_t enumTimer1CmpRegCpy)
{
    switch (enumTimer1CmpRegCpy)
    {
        case Timer1_cmp_OCR1A:
            Timer1_CB_CmpA = 0;
        break;

        case Timer1_cmp_ICR1:
            Timer1_CB_CmpIC = 0;
        break;

        case Timer1_cmp_OCR1B:
            Timer1_CB_CmpB = 0;
        break;
    }
}
// ********************* //

// *** overflow *** //
inline void Timer_vidRegisterCB_Overflow(const Timer_CB_t CBfuncCpy, const u8 u8TimerNCpy)
{
    switch (u8TimerNCpy)
    {
        case 0:
            Timer0_CB_Overflow = CBfuncCpy;
        break;

        case 2:
            Timer2_CB_Overflow = CBfuncCpy;
        break;
    }
}

inline void Timer1_vidRegisterCB_Overflow(const Timer_CB_t CBfuncCpy)
{
    Timer1_CB_Overflow = CBfuncCpy;
}

inline void Timer_vidDeregisterCB_Overflow(const u8 u8TimerNCpy)
{
    switch (u8TimerNCpy)
    {
        case 0:
            Timer0_CB_Overflow = 0;
        break;

        case 2:
            Timer2_CB_Overflow = 0;
        break;
    }
}

inline void Timer1_vidDeregisterCB_Overflow(void)
{
    Timer1_CB_Overflow = 0;
}
// **************** //
// ----------------- //


#ifndef RTOS_COOKIE_
ISR(TIMER0_COMP_vect) // called when a compare-match occurs
{
    if (Timer0_CB_Cmp) // if not empty (deregistered) function
        Timer0_CB_Cmp();
}
#endif

ISR(TIMER0_OVF_vect)  // called when overflow occurs (timer reaches Max value)
{
    if (Timer0_CB_Overflow) // if not empty (deregistered) function
        Timer0_CB_Overflow();
}


ISR(TIMER2_COMP_vect) // called when a compare-match occurs
{
    if (Timer2_CB_Cmp) // if not empty (deregistered) function
        Timer2_CB_Cmp();
}

ISR(TIMER2_OVF_vect)  // called when overflow occurs (timer reaches Max value)
{
    if (Timer2_CB_Overflow) // if not empty (deregistered) function
        Timer2_CB_Overflow();
}


ISR(TIMER1_COMPA_vect)
{
    if (Timer1_CB_CmpA) // if not empty (deregistered) function
        Timer1_CB_CmpA();
}

ISR(TIMER1_CAPT_vect)
{
    if (Timer1_CB_CmpIC) // if not empty (deregistered) function
        Timer1_CB_CmpIC();
}

ISR(TIMER1_COMPB_vect)
{
    if (Timer1_CB_CmpB) // if not empty (deregistered) function
        Timer1_CB_CmpB();
}

ISR(TIMER1_OVF_vect)
{
    if (Timer1_CB_Overflow) // if not empty (deregistered) function
        Timer1_CB_Overflow();
}

